//Создать класс родитель и 2 класса наследника.
class Parent {
    let name : String
    var age : Int
    
    init (name : String, age: Int) {
        self.name = name
        self.age = age
    }
}

class Child {
    let name = "Pablo"
    var age = 10
    let parent = Parent(name: "Alex", age: 35)
}

//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь), *destroy*(отображает что дом уничтожен).
class House {
    var width : Int
    var height : Int
    
    func create () {
        let multiply = width * height
        print("House area - \(multiply)")
    }
    func destroy () {
        height = 0
        width = 0
        print("House destroyed")
    }
    
    init (width : Int, height : Int) {
        self.width = width
        self.height = height
    }
}


let myHouse = House(width: 10, height: 30)
myHouse.create()
myHouse.destroy()
myHouse

//Создайте класс с методами, которые сортируют массив учеников по разным параметрам.
var array : [String] = ["Ivan", "Vladimir", "Timofey", "Anna", "Katya"]
class Methods {
    func sort1 (arr: [String]) -> [String] {
        let output = arr.sorted(by: {$0 < $1})
        return output
    }
    func sort2 (arr: [String]) -> [String] {
        let output = arr.sorted(by: {$0 > $1})
        return output
    }
}
var arr = Methods()
arr.sort1(arr: array)
arr.sort2(arr: array)

//Написать свою структуру и пояснить в комментариях - чем отличаются структуры от классов.
//Структуры передаются по значению, а классы — по ссылке.
struct Sample {
    var First = "test"
    var Second = 101
    var Third = true
    var Fourth : Int
}
//Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт. Сохраняйте комбинации в массив. Если выпала определённая комбинация - выводим соответствующую запись в консоль.

let deck = Array(1...52)

var hand = [Int]()

for _ in 1...5 {
    let card = deck.randomElement()!
    hand.append(card)
}

var isFlush = true
let suit = (hand.first! - 1) / 13
for card in hand {
    if (card - 1) / 13 != suit {
        isFlush = false
        break
    }
}

if isFlush {
    print("У вас флеш!")
} else {
    print("У вас не флеш")
}
